package metrix

import (
	kafka "github.com/segmentio/kafka-go.git"
)

// ServiceMessage is the json object sent on init
type ServiceMessage struct {
	Name        string  `json:"name"`
	Description *string `json:"description"`
	Namespace   *string `json:"namespace"`
}

// ServiceKey holds the key used to send services
const ServiceKey = "services"

// Init inits the metrix service
func (con *Connection) Init() error {

	switch con.Type {
	case Kafka:
		con.KafkaConnection = &KafkaConnection{
			Messages:  make(chan kafka.Message, 1024),
			Topic:     "metrix-message",
			Brokers:   con.Brokers,
			Partition: 0,
		}
		go con.KafkaConnection.KafkaWriter()
	case HTTP:
		break
	}

	if con.Health == true {
		con.ReportHealth()
	}

	err := con.sendInitMessage()
	if err != nil {
		return err
	}

	err = con.Infof("Service started [%s]", con.Service)

	return nil
}

func (con *Connection) sendInitMessage() error {
	var ns *string
	if con.Namespace == "" {
		ns = nil
	} else {
		ns = &con.Namespace
	}
	var desc *string
	if con.Description == "" {
		desc = nil
	} else {
		desc = &con.Description
	}
	msg := ServiceMessage{
		Name:        con.Service,
		Description: desc,
		Namespace:   ns,
	}

	// Start kafka instance

	return con.Send(ServiceKey, msg)
}
