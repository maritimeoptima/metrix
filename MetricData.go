package metrix

type MetricData struct {
	Service string  `json:"service"`
	Metric  string  `json:"metric"`
	Value   float64 `json:"value"`
}

const MetricDataKey string = "metric_data"
