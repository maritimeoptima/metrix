package metrix

import (
	"context"
	"errors"
	"log"
	"strconv"
	"time"

	kafka "github.com/segmentio/kafka-go.git"
)

// Connection create a new kafka connection
type KafkaConnection struct {
	Messages  chan kafka.Message
	Topic     string
	GroupID   string
	Brokers   []string
	Partition int
	MinBytes  *int
	MaxBytes  *int
	Timeout   *int
}

// KafkaWriter creates a worker group for kafka
func (con *KafkaConnection) KafkaWriter() {
	sleep := 10 // How many seconds to sleep after a timeout

	if con.Timeout != nil {
		sleep = *con.Timeout
	}

	sleepD := time.Duration(sleep) * time.Second
	for {
		kcon, err := kafka.DialLeader(context.Background(), "tcp", con.Brokers[0], con.Topic, con.Partition)
		if err != nil {
			log.Println(err, errors.New("(retrying in "+strconv.Itoa(sleep)+" seconds)"))
			time.Sleep(sleepD)
			continue
		}

		for msg := range con.Messages {
			_, err := kcon.WriteMessages(msg)
			if err != nil {
				break
			}
		}

		kcon.Close()
		log.Println("Kafka: connection broken (retrying in " + strconv.Itoa(sleep) + " seconds)")
		time.Sleep(sleepD)
	}
}
