package metrix

import "fmt"

// LogTypes is used for logtypes
type LogTypes string

const (
	// LogInfo is the info log type
	LogInfo LogTypes = "info"
	// LogWarning is the warning log type
	LogWarning LogTypes = "warning"
	// LogError is the error log type
	LogError LogTypes = "error"
)

// LogMessage contains the log message type
type LogMessage struct {
	Service string   `json:"service"`
	Message string   `json:"message"`
	Type    LogTypes `json:"type"`
}

// LogKey used when sending logs
const LogKey = "logs"

// Log logs a message
func (con *Connection) Log(message string, t LogTypes) error {
	msg := LogMessage{
		Service: con.Service,
		Message: message,
		Type:    t,
	}

	return con.Send(LogKey, msg)
}

// Info creates an info log object
func (con *Connection) Info(message string) error {
	return con.Log(message, LogInfo)
}

// Warning creates a warning log object
func (con *Connection) Warning(message string) error {
	return con.Log(message, LogWarning)
}

// Error creates an error log
func (con *Connection) Error(message string) error {
	return con.Log(message, LogError)
}

// Infof gives the ability to format messages
func (con *Connection) Infof(message string, args ...interface{}) error {
	m := fmt.Sprintf(message, args...)
	return con.Info(m)
}

// Warningf gives the ability to format messages
func (con *Connection) Warningf(message string, args ...interface{}) error {
	m := fmt.Sprintf(message, args...)
	return con.Warning(m)
}

// Errorf gives the ability to format messages
func (con *Connection) Errorf(message string, args ...interface{}) error {
	m := fmt.Sprintf(message, args...)
	return con.Error(m)
}
