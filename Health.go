package metrix

import (
	"time"
)

// HealthMessage contains the health message type
type HealthMessage struct {
	Service string    `json:"service"`
	Status  int       `json:"status"`
	Time    time.Time `json:"report_time"`
}

// HealthKey key used to // HealthKey key used to send health messages
const HealthKey = "health"

// SendHealth sends off a health messages to the broker
func (con *Connection) SendHealth() error {
	msg := HealthMessage{
		Service: con.Service,
		Status:  1,
		Time:    time.Now(),
	}

	return con.Send(HealthKey, msg)
}

// ReportHealth Will create an async handler to report the health to the broker on an interval
func (con *Connection) ReportHealth() error {
	// Try to send a health message first
	err := con.SendHealth()
	if err != nil {
		return err
	}
	interval := 10 * time.Second
	// Start async routine
	go repeatHealth(interval, con)
	return nil
}

func repeatHealth(d time.Duration, con *Connection) {
	for range time.Tick(d) {
		con.SendHealth()
	}
}
