package metrix

import (
	"encoding/json"

	kafka "github.com/segmentio/kafka-go.git"
)

// ConnectionTypes is used as an enum value for connection types
type ConnectionTypes int

const (
	// Kafka used to use metrix to send kafka messages
	Kafka ConnectionTypes = 0
	// HTTP use metrix in HTTP type
	HTTP ConnectionTypes = 1
)

// Connection metrix connection type
type Connection struct {
	Type            ConnectionTypes
	Namespace       string
	URL             *string // URL is only used in HTTP mode
	Service         string
	Description     string
	KafkaConnection *KafkaConnection
	Health          bool
	Brokers         []string
}

// Send will send data to the metrix logger
func (con *Connection) Send(key string, data interface{}) error {
	jsonData, err := json.Marshal(data)
	if err != nil {
		return err
	}

	switch con.Type {
	case Kafka:
		con.SendKafkaMessage(key, jsonData)
	case HTTP:
	}

	return nil
}

// SendKafkaMessage sends the kafka message to the broker
func (con *Connection) SendKafkaMessage(key string, data []byte) error {
	msg := kafka.Message{
		Key:   []byte(key),
		Value: data,
	}
	con.KafkaConnection.Messages <- msg

	// con.KafkaConnection.WriteMessages(msg)
	return nil
}
