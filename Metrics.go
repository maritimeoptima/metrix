package metrix

import (
	"fmt"
	"time"
)

// Metrics holds the metrics object
type Metrics struct {
	Service     string  `json:"service"`
	Name        string  `json:"name"`
	Description string  `json:"description"`
	UpperBound  float64 `json:"upper_bound"`
	LowerBound  float64 `json:"lower_bound"`
}

// MetricsConnection is a connection to a metric system
type MetricsConnection struct {
	Metric     *Metrics
	Connection *Connection
	Running    bool
}

// MetricsKey used to send metrics messages
const MetricsKey = "metrics"

// NewMetric creates a new metrics object
func (con *Connection) NewMetric(name, description string, upper, lower float64) (*MetricsConnection, error) {
	m := Metrics{
		Service:     con.Service,
		Name:        name,
		Description: description,
		UpperBound:  upper,
		LowerBound:  lower,
	}

	err := con.Send(MetricsKey, m)

	mc := MetricsConnection{
		Metric:     &m,
		Connection: con,
	}

	return &mc, err
}

// MetricsCallback is a callback type for metrics
type MetricsCallback func(metric *MetricsConnection)

// Start attaches a listener object to the metrics event
func (con *MetricsConnection) Start(cb MetricsCallback, interval int) {
	in := time.Duration(interval) * time.Second
	con.Running = true
	go repeatMetrics(cb, in, con)
}

func repeatMetrics(cb MetricsCallback, d time.Duration, con *MetricsConnection) {
	for range time.Tick(d) {
		if con.Running == false {
			fmt.Println("Stopping repeater")
			break
		}
		cb(con)
	}
}

// Stop stops the callback events
func (con *MetricsConnection) Stop() {
	con.Running = false
}

// Send sends data to the metrics connection
func (con *MetricsConnection) Send(value float64) error {
	md := MetricData{
		Metric:  con.Metric.Name,
		Value:   value,
		Service: con.Metric.Service,
	}

	return con.Connection.Send(MetricDataKey, &md)
}
